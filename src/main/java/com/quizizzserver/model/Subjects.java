package com.quizizzserver.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "subjects")
public class Subjects {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    public Subjects() {
    }

    public Subjects(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}

