package com.quizizzserver.service.impl;

import com.quizizzserver.model.Subjects;
import com.quizizzserver.repository.SubjectRepository;
import com.quizizzserver.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public List<Subjects> getAllSubject() {

        return subjectRepository.findAll();
    }
}
