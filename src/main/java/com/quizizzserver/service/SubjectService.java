package com.quizizzserver.service;

import com.quizizzserver.model.Subjects;

import java.util.List;


public interface SubjectService {

    public List<Subjects> getAllSubject();
}
