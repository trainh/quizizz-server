package com.quizizzserver.payload.respone;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class MessageResponse {
    private String message;
}
