package com.quizizzserver.payload.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Setter
@Getter
public class SignupRequest {

    @NotBlank
    private String username;

    @NotBlank
    @Email(message = "Email invalid !")
    private String email;

    @NotBlank
    private String password;

    private Set<String> role;
}
