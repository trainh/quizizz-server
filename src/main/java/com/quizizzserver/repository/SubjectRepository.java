package com.quizizzserver.repository;

import com.quizizzserver.model.Subjects;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubjectRepository extends JpaRepository<Subjects, Integer> {

    List<Subjects> findAll();
}
