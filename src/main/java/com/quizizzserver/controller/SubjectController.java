package com.quizizzserver.controller;

import com.quizizzserver.constans.SuccessCode;
import com.quizizzserver.dto.ResponseDTO;
import com.quizizzserver.exception.NotFoundException;
import com.quizizzserver.model.Subjects;
import com.quizizzserver.service.SubjectService;
import com.quizizzserver.service.impl.SubjectServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/subject")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @GetMapping("/get-subjects")
    public ResponseEntity<List<Subjects>> gteSubjects() {
        List<Subjects> list = null;
        try {
            list = subjectService.getAllSubject();
        } catch (Exception e) {
            throw new NotFoundException(e.getMessage());
        }
        return ResponseEntity.ok().body(list);
    }
}
