package com.quizizzserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuizizzServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuizizzServerApplication.class, args);
    }
}
